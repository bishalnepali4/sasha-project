from sqlalchemy import Column
from sqlalchemy.dialects.mysql import MEDIUMTEXT, VARCHAR, BOOLEAN, DATETIME, LONGTEXT, INTEGER
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class MemberTable(Base):
    '''Will change later

    '''
    id = Column(INTEGER, primary_key=True, autoincrement=True)
    internal_id = Column(INTEGER, unique=True)
    url = Column(VARCHAR(350), unique=True, nullable=False)

    publication_time = Column(DATETIME)
    has_video = Column(BOOLEAN)
    category = Column(MEDIUMTEXT)
    subcategory = Column(MEDIUMTEXT)
    title = Column(MEDIUMTEXT)
    subtitle = Column(MEDIUMTEXT)
    text = Column(LONGTEXT)
    main_image_url = Column(MEDIUMTEXT)


