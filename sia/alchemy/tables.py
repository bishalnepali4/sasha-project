from sqlalchemy import Column
from sqlalchemy.dialects.mysql import MEDIUMTEXT, VARCHAR, BOOLEAN, DATETIME, LONGTEXT, INTEGER
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Member(Base):

    __tablename__ = 'member'
    __table_args__ = {'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_bin'}

    id = Column(INTEGER, primary_key=True, autoincrement=True)
    internal_id = Column(INTEGER, unique=True, nullable=False)
    url = Column(VARCHAR(350))

    publication_time = Column(DATETIME)
    zip_code = Column(INTEGER)
    city = Column(MEDIUMTEXT)
    full_address = Column(LONGTEXT)
    gender = Column(MEDIUMTEXT)
    name = Column(MEDIUMTEXT)
    education = Column(MEDIUMTEXT)
    job = Column(MEDIUMTEXT)
    sector = Column(MEDIUMTEXT)
    group = Column(MEDIUMTEXT)
    section = Column(MEDIUMTEXT)
    tel = Column(MEDIUMTEXT)
    email = Column(MEDIUMTEXT)

def create_all(engine):
    print("creating databases")
    Base.metadata.create_all(engine)
