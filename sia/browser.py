# -*- coding: utf-8 -*-

# Copyright(C) 2018 Sasha Bouloudnine

from monseigneur.core.browser import PagesBrowser, URL
from pages import MemberList

__all__ = ['SiaBrowser']


class SiaBrowser(PagesBrowser):

    BASEURL = 'https://www.sia.ch/'

    list_members = URL("https://www.sia.ch/fr/affiliation/liste-des-membres/membres-individuels/nc/1/?tx_updsiafeuseradmin_pi1[displaySearchResult]=1&tx_updsiafeuseradmin_pi1[pointer]=(?P<page>\d+)", MemberList)

    def __init__(self, *args, **kwargs):
        super(SiaBrowser, self).__init__(*args, **kwargs)

    def iter_members(self, page):
        self.list_members.go(page=page)
        assert self.list_members.is_here()
        return self.page.iter_members()

if __name__ == '__main__':
    dm = SiaBrowser()
    dm.iter_members(page=2)