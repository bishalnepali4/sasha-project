from mbackend.core.fetcher import Fetcher
from mbackend.core.application import Application
from alchemy.dao_manager import DaoManager
from alchemy.tables import Member


class SiaBackend(Application):
    APPNAME = "Sia Scraping"
    VERSION = '1.0'
    COPYRIGHT = 'Copyright(C) 2012-YEAR LOBSTR'
    DESCRIPTION = "Scraping Backend for Leboncoin"
    SHORT_DESCRIPTION = "Step-by-step Example of Leboncoin Scraping"

    pass
