'''Returning the list of offices
'''

from with_requests import *
url_mapping = {
    'fr':'https://www.sia.ch/fr/affiliation/liste-des-membres/membres-bureaux/nc/1/?tx_updsiafeuseradmin_pi1[displaySearchResult]=1&tx_updsiafeuseradmin_pi1[pointer]='
}
if __name__ == '__main__':
    get_total_links(url='https://www.sia.ch/fr/affiliation/liste-des-membres/membres-bureaux/',language_='fr',OUTPUT_FILE='office_list.json',url_mapping=url_mapping)
    

