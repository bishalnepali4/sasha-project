
import requests
from lxml import html
import re
import json
import decrpty_contact


def get_html_page(url):
    res = requests.get(url)
    if res:
        html_page = html.fromstring(res.content)
    return html_page

# mapping for address
url_mapping = {
    'fr':'https://www.sia.ch/fr/affiliation/liste-des-membres/membres-individuels/nc/1/?tx_updsiafeuseradmin_pi1[displaySearchResult]=1&tx_updsiafeuseradmin_pi1[pointer]=',
    'en':'https://www.sia.ch/en/membership/member-directory/individual-members/nc/1/?tx_updsiafeuseradmin_pi1[displaySearchResult]=1&tx_updsiafeuseradmin_pi1[pointer]='
}
language_mapping ={
    'en':{
        'address':'Address',
        'contact':'Contact',
        'details':'Details',
    },
    'fr':{
'address':'Adresse',
        'contact':'Contact',
        'details':'Détails',
    }
}
details_mapping = {
    'en':{
        'job':'Profession',
        'sector':'Area of activity',
        'group':'Professional group',
        'section':'Section'
    },
    'fr':{
        'job':'Profession',
        'sector':'Domaine d’activité',
        'group':'Groupe professionnel',
        'section':'Section'
    }
}


def get_member_details(html_page,language_):
    '''
    '''
    #language_ = 'fr'
    text_file = html_page.xpath('//table/tr//text()')
    address = language_mapping.get(language_)['address']
    contact = language_mapping.get(language_)['contact']
    details = language_mapping.get(language_)['details']
    new_text_file = remove_nextline(text_file)
    address_index = new_text_file.index(address)
    contact_index = new_text_file.index(contact)
    details_index = new_text_file.index(details)
    address_text = new_text_file[address_index:contact_index]
    contact_text = new_text_file[contact_index:details_index]
    details_text = new_text_file[details_index:]
    member_details = {}
    member_details['address'] = address_text
    member_details['contact'] = contact_text
    member_details['details'] = details_text
    return member_details

def remove_nextline(list_name):
    '''Removing the \n line
    '''
    new_list = []
    for i in list_name:
        new_text = i.replace('\n','').strip()
        if new_text:
            new_list.append(new_text)
        else:
            pass
            
    return new_list
def get_total_links(url, language_,OUTPUT_FILE='member_lists.json',url_mapping = url_mapping):
    OUTPUT_FILE = OUTPUT_FILE
    total_links = []
    html_page = get_html_page(url)
    total_pages = get_total_entries(html_page)
    for i in range(0, total_pages):

        new_url = f'{url_mapping.get(language_)}{i}'
        print("New url is ", new_url)
        html_page = get_html_page(new_url)
        list_of_links = member_links(html_page)
        total_links.append(list_of_links)
        with open(OUTPUT_FILE, 'w') as df:
            json.dump({'links': total_links}, df,
                      indent=2, ensure_ascii=False)


def member_links(html_page):
    '''Returns the links of the member
    '''
    total_links = list()
    links_of_table = html_page.xpath('//table/tr')
    for link_of_table in links_of_table[1:]:
        details = {}
        list_of_row = []
        list_of_rows = link_of_table.xpath('.//td')


        details['name'] = remove_get_details(index=0, list_of_rows=list_of_rows, xpath_conf='.//a/text()')
        details['link'] = remove_get_details(index=0, list_of_rows=list_of_rows, xpath_conf='.//a/@href')
        details['professional'] = remove_get_details(index=2, list_of_rows=list_of_rows)
        details['zip_code'] = remove_get_details(index=3, list_of_rows=list_of_rows)
        details['city'] = remove_get_details(index=4, list_of_rows=list_of_rows)
        details['ro1'] = remove_get_details(index=5, list_of_rows=list_of_rows)
        details['wbn'] =remove_get_details(index=6, list_of_rows=list_of_rows)
        total_links.append(details)
    return total_links

def remove_get_details(index, list_of_rows, xpath_conf='./text()'):
    '''Removes the listing
    '''
    try:
        answer = list_of_rows[index].xpath(f'{xpath_conf}')[0]
    except:
        answer = None
    return answer

def get_total_entries(html_page):
    '''Get total entries
    '''
    text_entries = html_page.xpath('//h2/span/text()')[0]
    total_entries = ''.join(re.compile('\d+').findall(text_entries))
    total_pages = int(int(total_entries)/50 + 1)
    return total_pages

def get_details(details_list, language_):
    '''Return the list of the details
    '''
    details = {}
    job = details_mapping.get(language_)['job']
    sector = details_mapping.get(language_)['sector']
    group = details_mapping.get(language_)['group']
    section = details_mapping.get(language_)['section']
    try:
        job_index = details_list.index(job)
        job_text = details_list[job_index+1]
    except:
        job_text = None
    try:
        sector_index = details_list.index(sector)
        sector_text = details_list[sector_index+1]
    except:
        sector_text = None
    try:
        group_index = details_list.index(group)
        group_text = details_list[group_index+1]
    except:
        group_text = None
    try:
        section_index = details_list.index(section)
        section_text = details_list[section_index+1]
    except:
        sector_text = None
    details['JOB'] = job_text
    details['SECTOR'] = sector_text
    details['GROUP'] = group_text
    details['SECTION'] = section_text
    return details

def extract_contact(value_details):
    '''Extracting the number and email
    '''
    contact_details = {}
    contact_number = re.findall(r'\+[-()\s\d]+?(?=\s*[+<])', value_details)
    email_address = re.findall(r'[\w\.-]+@[\w\.-]+', str(value_details))
    if contact_number:
        contact_details['TEL'] = contact_number[0]
    if email_address:
        contact_details['EMAIL'] = email_address[0]
    return contact_details
def full_details(url_link,i):
    '''Full_details
    '''
    url = 'https://www.sia.ch'+url_link['link']
    print("Working on link",url)
    language = url_link['link'].split('/')[1]
    details = {}
    details['ZIP_CODE'] = url_link['zip_code']
    details['CITY'] = url_link['city']
    html_page = get_html_page(url)
    data_value = html_page.xpath('//div[@class="contact-data"]/@data-contact')[0]
    data_src = html_page.xpath('//div[@class="secr"]/@data-secr')[0]
    kwargs= {
        'secret_key':data_src,
        'data_value':data_value
    }
    member_details = get_member_details(html_page,language)
    value_details = decrpty_contact.get_contacts(**kwargs)
    contact_details = extract_contact(str(value_details))
    details['ID'] = i
    details['URL'] = url
    details['FULL_ADDRESS'] = ' '.join(member_details['address'][1:])
    details['GENDER'] = member_details['address'][1]
    details['NAME'] = member_details['address'][2]
    try:
        details['EDUCATION'] = member_details['address'][3]
    except:
         details['EDUCATION'] = None
    extract_details = get_details(member_details['details'], language)
    details.update(extract_details)
    details.update(contact_details)
    return details

def save_member_details():
    INPUT_FILE = 'member_list.json'
    OUTPUT_FILE = 'full_details.json'
    details = []
    i = 1
    with open(INPUT_FILE) as input_file:
        json_file = json.load(input_file)
        links_file = json_file['links']
        for list_file in links_file:
            for url_link in list_file:
                details.append(full_details(url_link,i))
                i = i+1
                with open(OUTPUT_FILE, 'w') as output_file:
                    json.dump({'links':details}, output_file, ensure_ascii=False, indent=2)
def save_member_list():
    OUTPUT_FILE = 'member_list.json'
    get_total_links(url='https://www.sia.ch/en/membership/member-directory/individual-members/',language_='fr',OUTPUT_FILE=OUTPUT_FILE,url_mapping=url_mapping)

if __name__ == '__main__':
    '''
    get_total_links(
        'https://www.sia.ch/fr/affiliation/liste-des-membres/membres-individuels/')
    '''
    #save_member_list()
    save_member_details()
    

    