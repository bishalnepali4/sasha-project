__all__ = ['show_e']

# Don't look below, you will not understand this Python code :) I don't.

from js2py.pyjs import *
# setting scope
var = Scope( JS_BUILTINS )
set_global_object(var)
id_required = 'nbjmup+jogpAcbnbsdi/di'
# Code follows:
var.registers(['decryptCharcode', 'a', 's', 'decryptString'])
@Js
def PyJsHoisted_decryptCharcode_(n, start, end, offset, this, arguments, var=var):
    var = Scope({'n':n, 'start':start, 'end':end, 'offset':offset, 'this':this, 'arguments':arguments}, var)
    var.registers(['offset', 'start', 'n', 'end'])
    var.put('n', (var.get('n')+var.get('offset')))
    if ((var.get('offset')>Js(0.0)) and (var.get('n')>var.get('end'))):
        var.put('n', (var.get('start')+((var.get('n')-var.get('end'))-Js(1.0))))
    else:
        if ((var.get('offset')<Js(0.0)) and (var.get('n')<var.get('start'))):
            var.put('n', (var.get('end')-((var.get('start')-var.get('n'))-Js(1.0))))
    return var.get('String').callprop('fromCharCode', var.get('n'))
PyJsHoisted_decryptCharcode_.func_name = 'decryptCharcode'
var.put('decryptCharcode', PyJsHoisted_decryptCharcode_)
@Js
def PyJsHoisted_decryptString_(enc, offset, this, arguments, var=var):
    var = Scope({'enc':enc, 'offset':offset, 'this':this, 'arguments':arguments}, var)
    var.registers(['enc', 'offset', 'n', 'i', 'dec', 'len'])
    var.put('dec', Js(''))
    var.put('len', var.get('enc').get('length'))
    #for JS loop
    var.put('i', Js(0.0))
    while (var.get('i')<var.get('len')):
        try:
            var.put('n', var.get('enc').callprop('charCodeAt', var.get('i')))
            if ((var.get('n')>=Js(43)) and (var.get('n')<=Js(58))):
                var.put('dec', var.get('decryptCharcode')(var.get('n'), Js(43), Js(58), var.get('offset')), '+')
            else:
                if ((var.get('n')>=Js(64)) and (var.get('n')<=Js(90))):
                    var.put('dec', var.get('decryptCharcode')(var.get('n'), Js(64), Js(90), var.get('offset')), '+')
                else:
                    if ((var.get('n')>=Js(97)) and (var.get('n')<=Js(122))):
                        var.put('dec', var.get('decryptCharcode')(var.get('n'), Js(97), Js(122), var.get('offset')), '+')
                    else:
                        var.put('dec', var.get('enc').callprop('charAt', var.get('i')), '+')
        finally:
                (var.put('i',Js(var.get('i').to_number())+Js(1))-Js(1))
    return var.get('dec')
PyJsHoisted_decryptString_.func_name = 'decryptString'
var.put('decryptString', PyJsHoisted_decryptString_)
pass
pass
var.put('s', Js(id_required))
var.put('a', var.get('decryptString')(var.get('s'), (-Js(1.0))))
var.get('console').callprop('log', var.get('a'))


# Add lib to the module scope
show_e = var.to_python()